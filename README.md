# Events Manager

Simple zeageorge/events_manager_7234

Fork for use with php version >=7.2.34

## Install

Via Composer

``` bash
$ composer require zeageorge/events_manager_7234
```

## Usage

``` php
<?php

namespace zeageorge\events_manager_7234\example;

use zeageorge\events_7234\Event;
use zeageorge\events_manager_7234\EventsManager;
use zeageorge\events_manager_7234\example\handlers\{
  OnApplicationStart2,
  OnApplicationEnd2
};

require '../vendor/autoload.php';

$eventsManager = new EventsManager(...require "events.php");

$eventsManager->hasEvent('onApplication_start') &&
  $eventsManager->getEvent('onApplication_start')->subscribe(new OnApplicationStart2()) &&
  $eventsManager->getEvent('onApplication_start')->setData(date('Y/m/d'))->publish();

echo "\ndo something...\n";

$eventsManager->hasEvent('onApplication_end') &&
  $eventsManager->getEvent('onApplication_end')->subscribe(new OnApplicationEnd2(), Event::DEFAULT_PRIORITY - 1) &&
  $eventsManager->getEvent('onApplication_end')->setData(date('Y/m/d'))->publish();


```

