<?php

declare(strict_types=1);

namespace zeageorge\events_manager_7234\example\handlers;

use zeageorge\events_7234\{
  Event,
  EventHandlerInterface
};

/**
 * Description of OnApplicationEnd
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class OnApplicationEnd implements EventHandlerInterface {
  /**
   *
   * @inheritdoc
   */
  public function handle(Event $event): void {
    echo "\n1: Application ended at {$event->getData()}\n";
  }
}
