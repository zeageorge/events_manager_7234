<?php

declare(strict_types=1);

namespace zeageorge\events_manager_7234\example\handlers;

use zeageorge\events_7234\{
  Event,
  EventHandlerInterface
};

/**
 * Description of OnApplicationStart2
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class OnApplicationStart2 implements EventHandlerInterface {
  /**
   *
   * @inheritdoc
   */
  public function handle(Event $event): void {
    echo "\n2: Application started at {$event->getData()}\n";
  }
}
