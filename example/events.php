<?php

declare(strict_types=1);

namespace zeageorge\events_manager_7234\example;

use zeageorge\events_7234\Event;
use zeageorge\events_manager_7234\example\handlers\{
  OnApplicationStart,
  OnApplicationEnd
};

return [
  (new Event('onApplication_start'))->subscribe(new OnApplicationStart()),
  (new Event('onApplication_end'))->subscribe(new OnApplicationEnd()),
];
