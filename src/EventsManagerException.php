<?php

declare(strict_types=1);

namespace zeageorge\events_manager_7234;

/**
 * Description of EventsManagerException
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class EventsManagerException extends \Exception {
}
