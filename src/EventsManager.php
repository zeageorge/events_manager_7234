<?php

declare(strict_types=1);

namespace zeageorge\events_manager_7234;

use InvalidArgumentException;
use zeageorge\events_7234\Event;
use function
  array_filter,
  array_diff_key,
  array_flip,
  in_array;

/**
 * Description of EventsManager
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class EventsManager {
  /** @var Event[] */
  protected $events = [];

  /**
   *
   * @param Event ...$events
   */
  public function __construct(Event ...$events) {
    $this->addEvents(...$events);
  }

  /**
   *
   * @param string $name
   * @return Event|null
   */
  public function getEvent(string $name): ?Event {
    return $this->events[$name] ?? null;
  }

  /**
   *
   * @param string ...$names
   * @return Event[]
   */
  public function getEvents(string ...$names): array {
    return array_filter($this->events, function (string $name) use ($names): bool {
      return in_array($name, $names);
    }, ARRAY_FILTER_USE_KEY);
  }

  /**
   *
   * @return Event[]
   */
  public function getAllEvents(): array {
    return $this->events;
  }

  /**
   *
   * @param string $name
   * @return bool
   */
  public function hasEvent(string $name): bool {
    return isset($this->events[$name]);
  }

  /**
   *
   * @param Event ...$events
   * @return self
   * @throws InvalidArgumentException
   */
  public function addEvents(Event ...$events): self {
    foreach ($events as $event) {
      $event_name = $event->getName();

      if (empty($event_name)) {
        throw new InvalidArgumentException("Invalid event name");
      }

      $this->events[$event_name] = $event;
    }

    return $this;
  }

  /**
   *
   * @param string ...$names
   * @return self
   */
  public function removeEvents(string ...$names): self {
    $this->events = array_diff_key($this->events, array_flip($names));
    return $this;
  }
}
